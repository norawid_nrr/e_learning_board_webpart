declare interface IELearningBoardWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'ELearningBoardWebPartStrings' {
  const strings: IELearningBoardWebPartStrings;
  export = strings;
}
