import * as React from 'react';
import Slider from "react-slick";
import styles from '../ELearningBoard.module.scss';
import '../ELearning.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface BadgeBoxProps {
  // style: React.CSSProperties;
}
interface BadgeBoxState {

}

export default class BadgeBox extends React.Component<BadgeBoxProps, {}> {

  public render(): React.ReactElement<BadgeBoxProps> {

    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
    };

    return (
      <div className="modalWrapper">
        <h2 style={{
          fontFamily: 'Nunito'
        }}>Badge</h2>
        <div style={{
          display: 'grid',
          textAlign: 'center'
        }}>
          <Slider {...settings}>
            <div>
              <img src="https://i.imgur.com/x98Hs53.png" alt="" className="imgBadge" />
              <div className="badgeTitle">Fast Leaner Badge </div>
              <div className="badgeDes">Completed 10 Courses in a month!</div>
            </div>
            <div>
              <img src="https://i.imgur.com/G9pT3gT.png" alt="" className="imgBadge" />
              <div className="badgeTitle">Knowledge Builder Badge </div>
              <div className="badgeDes">Created 1 course in the course catalogue </div>
            </div>
            <div>
              <img src="https://i.imgur.com/ySXDJPE.png" alt="" className="imgBadge" />
              <div className="badgeTitle">Newbie Badge </div>
              <div className="badgeDes">Congrats on your first log-in! </div>
            </div>
            <div>
              <img src="https://i.imgur.com/tc2BnRw.png" alt="" className="imgBadge" />
              <div className="badgeTitle">Super User Badge</div>
              <div className="badgeDes">Log-in everyday in a month! </div>
            </div>
          </Slider>
        </div>
      </div>
    );
  }
}
