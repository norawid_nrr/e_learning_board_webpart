import * as React from 'react';
import styles from '../ELearningBoard.module.scss';
import '../ELearning.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';

interface LeaderBoardProps {
  onClose: (event: React.MouseEvent<HTMLButtonElement>) => void;
  userName: ""[];
}
interface LeaderBoardState {
  userData: any[];
  rankData: any[];
  page: number;
  rowsPerPage: number;
}

export default class LeaderBoard extends React.Component<LeaderBoardProps, LeaderBoardState> {
  private scr;

  constructor(props: LeaderBoardProps) {
    super(props);
    this.state = {
      userData: [],
      rankData: [],
      page: 0,
      rowsPerPage: 100
    };

    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
  }

  private handleChangePage(event: unknown, newPage: number): void {
    this.setState({
      page: newPage
    });
  }

  private handleChangeRowsPerPage(event: React.ChangeEvent<HTMLInputElement>): void {

    this.setState({
      rowsPerPage: +event.target.value,
      page: 0
    });
  }

  private RetrieveSPData() {
    var reactHandler = this;

    var spRequest = new XMLHttpRequest();
    let urlGetItem = "/sites/BETTERTEST/_api/web/lists/getbytitle('UserInfo')/items?$select=OData__x0070_x72,Title,qnyu,mgrl&$top=5000&$orderby= mgrl desc";
    spRequest.open('GET', urlGetItem, true);
    spRequest.setRequestHeader("Accept", "application/json");

    spRequest.onreadystatechange = () => {

      if (spRequest.readyState === 4 && spRequest.status === 200) {
        var result = JSON.parse(spRequest.responseText);

        reactHandler.setState({
          userData: result.value
        });
      }
      else if (spRequest.readyState === 4 && spRequest.status !== 200) {
        console.log('Error Occured !');
      }
    };
    spRequest.send();

  }

  private getRankData() {
    var reactHandler = this;

    var spRequest = new XMLHttpRequest();
    let urlGetItem = "/sites/BETTERTEST/_api/web/lists/getbytitle('UserRank')/items?$select=OData__x0070_x72,Title,mgrl&$top=5000&$orderby= mgrl desc";
    spRequest.open('GET', urlGetItem, true);
    spRequest.setRequestHeader("Accept", "application/json");

    spRequest.onreadystatechange = () => {

      if (spRequest.readyState === 4 && spRequest.status === 200) {
        var result = JSON.parse(spRequest.responseText);

        reactHandler.setState({
          rankData: result.value
        });
      }
      else if (spRequest.readyState === 4 && spRequest.status !== 200) {
        console.log('Error Occured !');
      }
    };
    spRequest.send();

  }

  public componentDidMount() {
    this.RetrieveSPData();
    this.getRankData();
    // this.scr.scrollIntoView({ behavior: 'auto', block: 'center', inline: 'center' });
  }

  public render(): React.ReactElement<LeaderBoardProps> {

    const map = new Map();
    const { userData, rankData, page, rowsPerPage } = this.state;
    userData.forEach(item => map.set(item.Title, item));
    rankData.forEach(item => map.set(item.Title, { ...map.get(item.Title), ...item }));
    const mergedArr = Array.from(map.values());

    return (
      <div className="leaderTable">
        <div className="leaderTitle">
          <h2 style={{
            fontFamily: 'Nunito',
            textAlign: 'center',
            marginBottom: '.6rem'
          }}>Leaderboard</h2>
          <button onClick={this.props.onClose} style={{ alignSelf: 'baseline' }}>Close</button>
        </div>
        <div>
          <Paper>
            <TableContainer component={Paper} className="tableBox">
              <Table stickyHeader aria-label="simple table" style={{ fontFamily: 'Nunito' }} >
                <TableHead>
                  <TableRow>
                    <TableCell align="center">No.</TableCell>
                    <TableCell align="center">Avatar</TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Rank</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {mergedArr.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((data, index) => (
                    data.qnyu === `${this.props.userName}`
                      ?
                      <TableRow key={data.OData__x0070_x72} style={{ backgroundColor: '#30c0f0' }} >
                        <TableCell style={{ textAlign: 'center' }}>{index + 1}</TableCell>
                        <TableCell align="center">
                          <img src="https://image.freepik.com/free-vector/man-avatar-profile-round-icon_24640-14046.jpg" alt=""
                            style={{
                              maxHeight: 50,
                              maxWidth: 50,
                              borderRadius: '50%',
                              alignSelf: 'center'
                            }} />
                        </TableCell>
                        <TableCell style={{ textAlign: 'center' }}>{data.qnyu}</TableCell>
                        <TableCell style={{ textAlign: 'center' }}>{data.mgrl}</TableCell>
                      </ TableRow>
                      : < TableRow key={data.OData__x0070_x72} >
                        <TableCell style={{ textAlign: 'center' }}>{index + 1}</TableCell>
                        <TableCell align="center">
                          <img src="https://image.freepik.com/free-vector/man-avatar-profile-round-icon_24640-14046.jpg" alt=""
                            style={{
                              maxHeight: 50,
                              maxWidth: 50,
                              borderRadius: '50%',
                              alignSelf: 'center'
                            }} />
                        </TableCell>
                        <TableCell style={{ textAlign: 'center' }}>{data.qnyu}</TableCell>
                        <TableCell style={{ textAlign: 'center' }}>{data.mgrl}</TableCell>
                      </ TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100, 500, 1000]}
              component="div"
              count={mergedArr.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </Paper>
        </div>
      </div >
    );
  }
}
