import * as React from 'react';
import styles from '../ELearningBoard.module.scss';
import Modal from '@material-ui/core/Modal';
import BadgeBox from './BadgeBox';

interface IPointBoxProps {
  point: number[];
  filterCompleted: any[];
}
interface IPointBoxState {
  showBadge: boolean;
}

export default class PointBox extends React.Component<IPointBoxProps, IPointBoxState> {
  constructor(props: any) {
    super(props);
    this.state = {
      showBadge: false,
    };

    this.handleOpenBadge = this.handleOpenBadge.bind(this);
    this.handleCloseBadge = this.handleCloseBadge.bind(this);
  }

  private handleOpenBadge(): void {
    this.setState({
      showBadge: true
    });
  }

  private handleCloseBadge(): void {
    this.setState({
      showBadge: false
    });
  }


  public render(): React.ReactElement<IPointBoxProps> {

    return (
      <div className={styles.boxPointWrapper}>
        <div className={styles.pointTitle}>Your Achievement</div>
        <div className={styles.pointSection}>
          <div className={styles.pointWrapper}>
            <div className={styles.pointPie}>
              <div className={styles.pointCircle}>
                <div className={styles.pointIn}>{this.props.point}</div>
              </div>
              <button type="button" className={styles.btnBadge}>
                <div className={styles.pointTitle}>Learning Points</div>
              </button>
            </div>
            <div className={styles.pointPie}>
              <div className={styles.pointCircleBadge}>
                <div className={styles.pointIn}>4</div>
              </div>
              <button type="button" onClick={this.handleOpenBadge} className={styles.btnBadge}>
                <div className={styles.pointTitle}>Badges</div>
              </button>
            </div>
            <div className={styles.pointPie}>
              <div className={styles.pointCircleCom}>
                <div className={styles.pointIn}>{this.props.filterCompleted.length}</div>
              </div>
              <button type="button" className={styles.btnBadge}>
                <div className={styles.pointTitle}>Completed <br /> Courses</div>
              </button>
            </div>
            <div className={styles.pointPie}>
              <div className={styles.pointCircleCoin}>
                <div className={styles.pointIn}>0</div>
              </div>
              <button type="button" className={styles.btnBadge}>
                <div className={styles.pointTitle}>Coins</div>
              </button>
            </div>
          </div>
          <div className={styles.coinWrapper}>
            <div className={styles.coinTitle}>Total of Coins</div>
            <div className={styles.gainCoinWrapper}>
              <div className={styles.coinImg}>
                <img src="https://www.flaticon.com/svg/static/icons/svg/771/771275.svg" alt="" className={styles.chest} />
              </div>
              <div className={styles.coinText}>5 PLEARN Diamond Coins</div>
            </div>
            <div className={styles.gainCoinWrapper}>
              <div className={styles.coinImg}>
                <img src="https://www.flaticon.com/svg/static/icons/svg/771/771266.svg" alt="" className={styles.chest} />
              </div>
              <div className={styles.coinText}>15 PLEARN Gold Coins</div>
            </div>
          </div>
        </div>

        <Modal
          open={this.state.showBadge}
          onClose={this.handleCloseBadge}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          className={styles.modalWrapper}
        >
          <div className={styles.modalWrapper}>
            <BadgeBox />
          </div>
        </Modal>
      </div>
    );
  }
}
