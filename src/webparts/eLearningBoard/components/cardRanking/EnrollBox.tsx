import * as React from 'react';
import styles from '../ELearningBoard.module.scss';
import * as moment from 'moment';

interface IProjectBoxProps {
  enrollCourse: any[];
}
interface IProjectBoxState {
}

export default class ProjectBox extends React.Component<IProjectBoxProps, IProjectBoxState> {

  constructor(props: any) {
    super(props);
  }

  public render(): React.ReactElement<IProjectBoxProps> {

    return (
      <div className={styles.boxProjectWrapper}>
        <div className={styles.enrollTitle}>3 Latest Enrollment</div>
        <div className={styles.processWrapper}>

          {/* Enrollment set */}
          {this.props.enrollCourse.map((data, index) => (
            data.k7x4 === 'NotStarted'
              ?
              <div className={styles.courseProcess}>
                <div className={styles.processFlag}>
                  <div className={styles.flagPercentLead}>
                    <div className={styles.flagArrowLead}>
                      <div className={styles.percentIn}></div>
                    </div>
                  </div>
                  <div className={styles.flagInfo}>
                    <ul>
                      <li className={styles.titleTop}>Title: {data.g56t}</li>
                      <li className={styles.status}>Status:&nbsp;<div className={styles.enrollment}> Enrolled</div></li>
                      <li>Start Date: - </li>
                      <li>Completed Date: - </li>
                    </ul>
                  </div>
                  <div className={styles.flagTextLead}>{data.g56t.length > 10 ? `${data.g56t.substring(0, 10)}...` : data.g56t}</div>
                </div>
                <div className={styles.processBar}>
                  <div className={styles.processDivide}>
                    <div className={styles.processCourseLead}></div>
                    <div className={styles.processCourseLead}></div>
                    <div className={styles.processCourseLead}></div>
                  </div>
                </div>
              </div>
              :
              data.k7x4 === 'InProgress'
                ?
                <div className={styles.courseProcess}>
                  <div className={styles.processFlag}>
                    <div className={styles.flagPercentGami}>
                      <div className={styles.flagArrowGami}>
                        <div className={styles.percentIn}></div>
                      </div>
                    </div>
                    <div className={styles.flagInfo}>
                      <ul>
                        <li className={styles.titleTop}>Title: {data.g56t}</li>
                        <li className={styles.status}>Status:&nbsp;<div className={styles.inProcess}> In-Progress</div></li>
                        <li>Start Date: {moment(data.OData__x006f_aa3).format('DD/MM/YYYY')}</li>
                        <li>Completed Date: - </li>
                      </ul>
                    </div>
                    <div className={styles.flagTextGami}>{data.g56t.length > 10 ? `${data.g56t.substring(0, 10)}...` : data.g56t}</div>
                  </div>
                  <div className={styles.processBar}>
                    <div className={styles.processDivide}>
                      <div className={styles.processCourseGami}></div>
                      <div className={styles.processCourseGami}></div>
                      <div className={styles.processCourseGami}></div>
                    </div>
                  </div>
                </div>
                :
                data.k7x4 === 'Completed'
                  ?
                  <div className={styles.courseProcess}>
                    <div className={styles.processFlag}>
                      <div className={styles.flagPercentStory}>
                        <div className={styles.flagArrowStory}>
                          <div className={styles.percentIn}></div>
                        </div>
                      </div>
                      <div className={styles.flagInfo}>
                        <ul>
                          <li className={styles.titleTop}>Title: {data.g56t}</li>
                          <li className={styles.status}>Status:&nbsp;<div className={styles.complete}> Complete</div></li>
                          <li>Start Date: {moment(data.OData__x006f_aa3).format('DD/MM/YYYY')}</li>
                          <li>Completed Date: {moment(data.z6yp).format('DD/MM/YYYY')}</li>
                        </ul>
                      </div>
                      <div className={styles.flagTextStory}>{data.g56t.length > 10 ? `${data.g56t.substring(0, 10)}...` : data.g56t}</div>
                    </div>
                    <div className={styles.processBar}>
                      <div className={styles.processDivide}>
                        <div className={styles.processCourseStory}></div>
                        <div className={styles.processCourseStory}></div>
                        <div className={styles.processCourseStory}></div>
                      </div>
                    </div>
                  </div>
                  :
                  <div className={styles.courseProcess}>
                    No Enrollment
                </div>
          ))}
        </div>
      </div>
    );
  }
}
