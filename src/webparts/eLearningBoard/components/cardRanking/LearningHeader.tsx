import * as React from 'react';
import styles from '../ELearningBoard.module.scss';
import CertificateBox from './CertificateBox';
import Modal from '@material-ui/core/Modal';
import LeaderBoard from './LeaderBoard';
import * as jquery from 'jquery';
import 'react-responsive-modal/styles.css';

interface ILearningHeaderProps {
}

interface ILearningHeaderState {
  showCer: boolean;
  showLeader: boolean;
  timeStatus: string;
  currentUser: string;
  items: [{}];
  userData: [{
    userId: "",
    userLoginName: "",
    userName: "",
    rank: "",
    email: "",
    ceu: ""
  }];
}

export default class LearningHeader extends React.Component<ILearningHeaderProps, ILearningHeaderState> {
  private intervalHandle: any;

  constructor(props: any) {
    super(props);
    this.state = {
      showCer: false,
      showLeader: false,
      timeStatus: '',
      currentUser: "",
      items: [{}],
      userData: [{
        userId: "",
        userLoginName: "",
        userName: "",
        rank: "",
        email: "",
        ceu: ""
      }]
    };

    this.handleOpenCer = this.handleOpenCer.bind(this);
    this.handleCloseCer = this.handleCloseCer.bind(this);
    this.handleOpenLeader = this.handleOpenLeader.bind(this);
    this.handleCloseLeader = this.handleCloseLeader.bind(this);
  }

  private handleOpenCer(): void {
    this.setState({
      showCer: true
    });
  }

  private handleCloseCer(): void {
    this.setState({
      showCer: false
    });
  }

  private handleOpenLeader(): void {
    this.setState({
      showLeader: true
    });
  }

  private handleCloseLeader(): void {
    this.setState({
      showLeader: false
    });
  }

  private getCurrentUser() {

    let spRequest = new XMLHttpRequest();
    let urlCurrent = "/sites/BETTERTEST/_api/web/currentuser";

    spRequest.open('GET', urlCurrent, true);
    spRequest.setRequestHeader("Accept", "application/json");

    spRequest.onreadystatechange = () => {

      if (spRequest.readyState === 4 && spRequest.status === 200) {
        let result = JSON.parse(spRequest.responseText);

        this.RetrieveSPData("UserInfo", result.LoginName);
      }
      else if (spRequest.readyState === 4 && spRequest.status !== 200) {
        console.log('Error Occured !');
      }
    };
    spRequest.send();
  }

  private RetrieveSPData(listname, loginName) {

    let reactHandler = this;

    var spRequest = new XMLHttpRequest();
    let encode = encodeURIComponent(loginName);
    let urlGetItem = "/sites/BETTERTEST/_api/web/lists/getbytitle('" + listname + "')/items"
      + "?$select=OData__x0070_x72,Title,qnyu,mgrl,s1r3,cdba&$filter=Title+eq+'" + encode + "'";
    spRequest.open('GET', urlGetItem, true);
    spRequest.setRequestHeader("Accept", "application/json");

    spRequest.onreadystatechange = () => {

      if (spRequest.readyState === 4 && spRequest.status === 200) {
        var result = JSON.parse(spRequest.responseText);

        reactHandler.setState({
          items: result.value,
          userData: [{
            userId: result.value.map(data => data.OData__x0070_x72),
            userLoginName: result.value.map(data => data.Title),
            userName: result.value.map(data => data.qnyu),
            rank: result.value.map(data => data.mgrl),
            email: result.value.map(data => data.s1r3),
            ceu: result.value.map(data => data.cdba)
          }]
        });
      }
      else if (spRequest.readyState === 4 && spRequest.status !== 200) {
        console.log('Error Occured !');
      }
    };
    spRequest.send();

  }

  public componentDidMount() {

    this.getCurrentUser();

    this.intervalHandle = setInterval(() => {

      const date = new Date;
      let hours = date.getHours();
      let mins = date.getMinutes();
      let minString;

      if (mins < 10) {
        minString = `0${mins}`;
      } else {
        minString = mins;
      }

      let time = parseInt(`${hours}${minString}`);

      if (time > 500 && time < 1159) {
        this.setState({
          timeStatus: 'Good Morning, '
        });
      } else if (time > 1200 && time < 1659) {
        this.setState({
          timeStatus: 'Good Afternoon, '
        });
      } else {
        this.setState({
          timeStatus: 'Good Evening, '
        });
      }

      // console.log(time);
    }, 1000);
  }

  public componentWillUnmount() {
    clearInterval(this.intervalHandle);
  }

  public render(): React.ReactElement<ILearningHeaderProps> {

    const { timeStatus, userData } = this.state;
    const userName = userData.map(data => data.userName);
    const rank = userData.map(data => data.rank);

    return (
      <div className={styles.headerWrapper} >
        <div className={styles.avatarWrapper}>
          <div className={styles.avatarText}>{timeStatus}{userName}</div>
          <div className={styles.avatarImg}>
            <img src="https://img.favpng.com/17/1/20/user-profile-computer-icons-login-png-favpng-w4PDSTJJhQAyy1K6Dbdyjx4XW.jpg" alt="" className={styles.avatar} />
          </div>
        </div>
        <div className={styles.rankWrapper}>
          <div className={styles.rankText}>Your Rank : {rank} {/* Novice */}</div>
          <div className={styles.rankImg}>
            <img src="https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/512x512/medal.png" alt="" className={styles.rank} />
            <div className={styles.rankTitle}>Beginner</div>
          </div>
        </div>
        <div className={styles.levelWrapper}>
          <div className={styles.levelText}>Your Level : Novice LV.2 </div>
          <div className={styles.levelBox}>
            <div className={styles.levelPop}>
              <div className={styles.level}>
                Newbie
              </div>
            </div>
          </div>
        </div>
        <div className={styles.boardWrapper}>
          <button type="button" onClick={this.handleOpenLeader} >
            <div className={styles.boardText}>
              Leaderboard (Bank-wided)
              </div>
          </button>
          <div className={styles.boardImg}>
            <img src="https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/512x512/podium2.png" alt="" className={styles.board} />
          </div>
        </div>
        <div className={styles.cerWrapper}>
          <button type="button" onClick={this.handleOpenCer} >
            <div className={styles.certText}>
              My Certificate
            </div>
          </button>
        </div>

        <Modal
          open={this.state.showLeader}
          onClose={this.handleCloseLeader}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <div className={styles.modalWrapper}>
            <LeaderBoard onClose={this.handleCloseLeader} userName={userName} />
          </div>
        </Modal>

        <Modal
          open={this.state.showCer}
          onClose={this.handleCloseCer}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <div className={styles.modalWrapper}>
            <CertificateBox />
          </div>
        </Modal>
      </div>
    );
  }
}
