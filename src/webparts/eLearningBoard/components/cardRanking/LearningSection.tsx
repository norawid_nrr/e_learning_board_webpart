import * as React from 'react';
import styles from '../ELearningBoard.module.scss';
import Point from './PointBox';
import Enroll from './EnrollBox';
import Gami from './GamiBox';
import * as jquery from 'jquery';
import { WebPartContext } from '@microsoft/sp-webpart-base';

interface ILearningSectionProps {
  context: WebPartContext;
}
interface ILearningSectionState {
  currentUser: string;
  userData: [{
    userId: "",
    userLoginName: "",
    userName: "",
    ceu: number
  }];
  courseData: any[];
  enrollData: any[];
  pointData: any[];
}

export default class LearningSection extends React.Component<ILearningSectionProps, ILearningSectionState> {

  public context: WebPartContext;
  public siteUrl = this.props.context.pageContext.web.absoluteUrl;
  public response = [];
  public url = this.siteUrl + "/_api/web/lists/getbytitle('UserEnroll')/items"
    + "?$select=dx2i,OData__x0071_nj0,tfku,k7x4,OData__x006f_aa3,z6yp"
    + "&$skiptoken=Paged=TRUE&$top=4999";

  constructor(props: any) {
    super(props);
    this.state = {
      currentUser: "",
      userData: [{
        userId: "",
        userLoginName: "",
        userName: "",
        ceu: 0
      }],
      courseData: [],
      enrollData: [],
      pointData: []
    };

  }

  private getCurrentUser() {

    let spRequest = new XMLHttpRequest();
    let urlCurrent = "/sites/BETTERTEST/_api/web/currentuser";

    spRequest.open('GET', urlCurrent, true);
    spRequest.setRequestHeader("Accept", "application/json");

    spRequest.onreadystatechange = () => {

      if (spRequest.readyState === 4 && spRequest.status === 200) {
        let result = JSON.parse(spRequest.responseText);

        this.setState({
          currentUser: result.LoginName
        });

        this.RetrieveSPData("UserInfo", result.LoginName);
        this.getCourseData();
        this.getEnrollData();

      }
      else if (spRequest.readyState === 4 && spRequest.status !== 200) {
        console.log('Error Occured !');
      }
    };
    spRequest.send();
  }

  private RetrieveSPData(listname, loginName) {

    let reactHandler = this;

    var spRequest = new XMLHttpRequest();
    let encode = encodeURIComponent(loginName);
    let urlGetItem = "/sites/BETTERTEST/_api/web/lists/getbytitle('" + listname + "')/items"
      + "?$select=OData__x0070_x72,Title,qnyu,cdba&$filter=Title+eq+'" + encode + "'";
    spRequest.open('GET', urlGetItem, true);
    spRequest.setRequestHeader("Accept", "application/json");

    spRequest.onreadystatechange = () => {

      if (spRequest.readyState === 4 && spRequest.status === 200) {
        var result = JSON.parse(spRequest.responseText);

        reactHandler.setState({
          userData: [{
            userId: result.value.map(data => data.OData__x0070_x72),
            userLoginName: result.value.map(data => data.Title),
            userName: result.value.map(data => data.qnyu),
            ceu: result.value.map(data => data.cdba)
          }]
        });
      }
      else if (spRequest.readyState === 4 && spRequest.status !== 200) {
        console.log('Error Occured !');
      }
    };
    spRequest.send();

  }

  private getEnrollData() {
    let reactHandler = this;

    jquery.ajax({
      url: reactHandler.url,
      method: "GET",
      headers: {
        "Accept": "application/json; odata=verbose"
      },
      success: (data) => {
        reactHandler.response = reactHandler.response.concat(data.d.results);
        if (data.d.__next) {
          reactHandler.url = data.d.__next;
          reactHandler.getEnrollData();
          // console.log(reactHandler.response);
        }
        else {
          reactHandler.setState({
            enrollData: reactHandler.response
          });
        }
      },
      error: (error) => {
        console.log(error);
      }
    });
  }

  private getCourseData() {

    let reactHandler = this;

    var spRequest = new XMLHttpRequest();
    let urlGetItem = "/sites/BETTERTEST/_api/web/lists/getbytitle('UserCourse')/items"
      + "?$select=Title,g56t"
      + "&$skiptoken=Paged=TRUE&$top=4999";
    spRequest.open('GET', urlGetItem, true);
    spRequest.setRequestHeader("Accept", "application/json");

    spRequest.onreadystatechange = () => {

      if (spRequest.readyState === 4 && spRequest.status === 200) {
        var result = JSON.parse(spRequest.responseText);

        reactHandler.setState({
          courseData: result.value
        });
      }
      else if (spRequest.readyState === 4 && spRequest.status !== 200) {
        console.log('Error Occured !');
      }
    };
    spRequest.send();

  }

  public componentDidMount() {

    this.getCurrentUser();

  }

  public render(): React.ReactElement<ILearningSectionProps> {

    const map = new Map();
    const { userData, courseData, enrollData, currentUser } = this.state;
    enrollData.forEach(item => map.set(item.tfku, item));
    courseData.forEach(item => map.set(item.Title, { ...map.get(item.Title), ...item }));
    const mergedArr = Array.from(map.values());
    const filterEnroll = mergedArr.filter(filter => filter.OData__x0071_nj0 === currentUser);
    const filterCompleted = mergedArr.filter(filter => filter.OData__x0071_nj0 === currentUser && filter.k7x4 === 'Completed');
    const point = userData.map(data => data.ceu);

    console.log(filterEnroll);

    return (
      <div>
        <div className={styles.sectionWrapper}>
          <Enroll enrollCourse={filterEnroll} />
          <Gami />
          <Point filterCompleted={filterCompleted} point={point} />
        </div>
      </div >
    );
  }
}
