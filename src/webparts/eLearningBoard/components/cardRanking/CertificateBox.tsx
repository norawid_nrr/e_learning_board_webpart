import * as React from 'react';
import Slider from "react-slick";
import styles from '../ELearningBoard.module.scss';
import '../ELearning.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface CertificateBoxProps {
  // style: React.CSSProperties;
}
interface CertificateBoxState {

}

export default class CertificateBox extends React.Component<CertificateBoxProps, {}> {

  public render(): React.ReactElement<CertificateBoxProps> {

    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    return (
      <div className="modalWrapper">
        <h2 style={{
          fontFamily: 'Nunito'
        }}>Certificate</h2>
        <div className={styles.slideWrapper}>
          <Slider {...settings}>
            <div>
              <img src="https://binaries.templates.cdn.office.net/support/templates/en-us/lt04027254_quantized.png" alt="" className="imgCer" />
            </div>
            <div>
              <img src="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/gift-certificate-card-template-28e6adfd7e7959e5cc3d48f67f48e337_screen.jpg?ts=1561536797" alt="" className="imgCer" />
            </div>
            <div>
              <img src="https://dzf8vqv24eqhg.cloudfront.net/userfiles/2086/3660/ckfinder/images/7(4).jpg" alt="" className="imgCer" />
            </div>
            <div>
              <img src="https://i.ytimg.com/vi/KDqoYyCeQ3I/maxresdefault.jpg" alt="" className="imgCer" />
            </div>
            <div>
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSLkYQZlBrVsOr-q_L8L_4FaM4ROP_nOc4G9A&usqp=CAU" alt="" className="imgCer" />
            </div>
            <div>
              <img src="https://www.msit.mut.ac.th/wp-content/uploads/2020/05/cert_sample-1024x724.jpg" alt="" className="imgCer" />
            </div>
          </Slider>
        </div>
      </div>
    );
  }
}
