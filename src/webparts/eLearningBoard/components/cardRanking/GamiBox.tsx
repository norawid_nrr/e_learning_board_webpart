import * as React from 'react';
import styles from '../ELearningBoard.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTasks, faCheck, faPenAlt, faGraduationCap } from '@fortawesome/free-solid-svg-icons';

interface IGamiNewProps {

}
interface IGamiNewState {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
  hoverCom: boolean;
  hoverInPro: boolean;
  hoverEn: boolean;
}

export default class GamiNew extends React.Component<IGamiNewProps, IGamiNewState> {
  private intervalHandle: any;

  constructor(props: IGamiNewProps) {
    super(props);
    this.state = {
      days: 22,
      hours: 4,
      minutes: 19,
      seconds: 25,
      hoverCom: false,
      hoverInPro: false,
      hoverEn: false
    };

    this.handleOnHoverCom = this.handleOnHoverCom.bind(this);
    this.handleOffHoverCom = this.handleOffHoverCom.bind(this);
    this.handleOnHoverInPro = this.handleOnHoverInPro.bind(this);
    this.handleOffHoverInPro = this.handleOffHoverInPro.bind(this);
    this.handleOnHoverEn = this.handleOnHoverEn.bind(this);
    this.handleOffHoverEn = this.handleOffHoverEn.bind(this);
  }

  private handleOnHoverCom() {
    this.setState({
      hoverCom: true
    });
  }

  private handleOffHoverCom() {
    this.setState({
      hoverCom: false
    });
  }

  private handleOnHoverInPro() {
    this.setState({
      hoverInPro: true
    });
  }

  private handleOffHoverInPro() {
    this.setState({
      hoverInPro: false
    });
  }

  private handleOnHoverEn() {
    this.setState({
      hoverEn: true
    });
  }

  private handleOffHoverEn() {
    this.setState({
      hoverEn: false
    });
  }

  public componentDidMount() {
    this.intervalHandle = setInterval(() => {
      const { seconds, minutes, hours, days } = this.state;

      if (seconds > 0) {
        this.setState({
          seconds: seconds - 1
        });
      }

      if (seconds === 0) {
        if (minutes === 0) {
          if (hours === 0) {
            if (days === 0) {
              clearInterval(this.intervalHandle);
            } else {
              this.setState({
                days: days - 1,
                hours: 23,
                minutes: 59,
                seconds: 59
              });
            }
          } else {
            this.setState({
              hours: hours - 1,
              minutes: 59,
              seconds: 59
            });
          }
        } else {
          this.setState({
            minutes: minutes - 1,
            seconds: 59
          });
        }
      }
    }, 1000);
  }

  public componentWillUnmount() {
    clearInterval(this.intervalHandle);
  }

  public render(): React.ReactElement<IGamiNewProps> {

    const { seconds, minutes, hours, days } = this.state;

    return (
      <div className={styles.boxCourseWrapper}>
        <div className={styles.courseWrapper}>
          <div className={styles.courseTitle}>
            Campaign
            <div className={styles.countWrapper}>
              {days === 0 && hours === 0 && minutes === 0 && seconds === 0
                ? <div className={styles.noCountDown}>No Campaign !</div>
                : <div className={styles.countDown}>
                  {days} Days {hours} Hrs {minutes} Mins {seconds} Secs
                      <div className={styles.timeLeft}>&nbsp;Lefts</div>
                </div>
              }
            </div>
          </div>
          <div className={styles.courseCardWrapper}>
            <div className={styles.courseCard}>
              <div className={styles.skillStats}>
                <div className={styles.stepWrapper}>
                  <div className={styles.hexWrapper} onMouseEnter={this.handleOnHoverCom} onMouseLeave={this.handleOffHoverCom}>
                    <div className={this.state.hoverCom === true ? [styles.hexTextStart, styles.highlight].join(' ') : styles.hexTextStart}>Completed
                      <div className={styles.desText}> 2 of 5 Completed Courses</div>
                    </div>
                    <div className={this.state.hoverCom === true ? [styles.hexagon, styles.inTime].join(' ') : styles.hexagon}>
                      <div className={[styles.hexagon, styles.inSpace].join(' ')}>
                        <div className={[styles.hexagon, styles.innerCom].join(' ')}>
                          <FontAwesomeIcon icon={this.state.hoverCom === true ? faCheck : faGraduationCap} />
                        </div>
                      </div>
                    </div>
                    <div className={styles.hexTextEnd}></div>
                  </div>
                  <div className={styles.hexWrapper} onMouseEnter={this.handleOnHoverInPro} onMouseLeave={this.handleOffHoverInPro}>
                    <div className={styles.hexTextStart}></div>
                    <div className={this.state.hoverInPro === true ? [styles.hexagon, styles.inTime].join(' ') : styles.hexagon}>
                      <div className={[styles.hexagon, styles.inSpace].join(' ')}>
                        <div className={[styles.hexagon, styles.innerInPro].join(' ')}>
                          <FontAwesomeIcon icon={this.state.hoverInPro === true ? faCheck : faPenAlt} />
                        </div>
                      </div>
                    </div>
                    <div className={this.state.hoverInPro === true ? [styles.hexTextEnd, styles.highlight].join(' ') : styles.hexTextEnd}>
                      In-Progress
                      <div className={styles.desText}> 1 of 5 In-Process Courses</div>
                    </div>
                  </div>
                  <div className={styles.hexWrapper} onMouseEnter={this.handleOnHoverEn} onMouseLeave={this.handleOffHoverEn}>
                    <div className={this.state.hoverEn === true ? [styles.hexTextStart, styles.highlight].join(' ') : styles.hexTextStart}>
                      Enrolled
                      <div className={styles.desText}> 2 of 5 Enrolled Courses</div>
                    </div>
                    <div className={this.state.hoverEn === true ? [styles.hexagon, styles.inTime].join(' ') : styles.hexagon}>
                      <div className={[styles.hexagon, styles.inSpace].join(' ')}>
                        <div className={[styles.hexagon, styles.innerEn].join(' ')}>
                          <FontAwesomeIcon icon={this.state.hoverEn === true ? faCheck : faTasks} />
                        </div>
                      </div>
                    </div>
                    <div className={styles.hexTextEnd}></div>
                  </div>
                </div>
              </div>
            </div>
            {/* <div className={styles.skillPoints}>
              <div className={styles.pointSkillWrapper}>
                <span className={styles.pointsEn} />
                <div className={styles.pointsText}>Enrolled</div>
              </div>
              <div className={styles.pointSkillWrapper}>
                <span className={styles.pointsPro} />
                <div className={styles.pointsText}>In-Progress</div>
              </div>
              <div className={styles.pointSkillWrapper}>
                <span className={styles.pointsCom} />
                <div className={styles.pointsText}>Completed</div>
              </div>
            </div> */}
          </div>
        </div>
      </div >
    );
  }
}
