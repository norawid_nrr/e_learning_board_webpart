import { WebPartContext } from "@microsoft/sp-webpart-base";

export interface IELearningBoardProps {
  description: string;
  context: WebPartContext;
  siteurl: string;
}
