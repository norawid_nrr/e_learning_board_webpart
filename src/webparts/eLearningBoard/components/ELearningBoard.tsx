import * as React from 'react';
import styles from './ELearningBoard.module.scss';
import { IELearningBoardProps } from './IELearningBoardProps';
import LearningHeader from './cardRanking/LearningHeader';
import LearningSection from './cardRanking/LearningSection';

export default class ELearningBoard extends React.Component<IELearningBoardProps, {}> {
  public render(): React.ReactElement<IELearningBoardProps> {
    return (
      <div className={styles.eLearningBoard}>
        <div className={styles.container}>
          <div className={styles.cardWrapper}>
            <LearningHeader />
            <LearningSection context={this.props.context} />
          </div>
        </div>
      </div>
    );
  }
}
